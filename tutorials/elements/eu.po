msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2009-03-09 17:55+0100\n"
"Last-Translator: Ales Zabala Alava (Shagi) <shagi@gisa-elkartea.org>\n"
"Language-Team: Basque <translation-team-eu@lists.sourceforge.net>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Ales Zabala Alava (Shagi) <shagi@gisa-elkartea.org>, 2009\n"
"Maider Likona Santamarina <pisoni@gisa-elkartea.org>, 2009"

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
#, fuzzy
msgid "Elements of design"
msgstr "Diseinuaren elementuak"

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Tutorial honek diseinuaren elementu eta arauak azaltzen ditu, normalean "
"arteko ikasleei hasieran erakusten zaienak artea egitean erabiltzen diren "
"hainbat propietate ulertzeko. Hau ez da zerrenda oso bat, beraz tutorial hau "
"ulergarriago egitearren nahi duzuna gehitu, kendu eta konposa ezazu."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr "Diseinuaren elementuak"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr "Ondorengo elementuak diseinuaren oinarrizko adreiluak dira."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:132
#, no-wrap
msgid "Line"
msgstr "Lerroa"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"Lerro bat luzera eta norabidea duen marka bat bezala definitzen da, gainazal "
"batean puntu bat mugituz lortzen dena. Lerro bat luzeran, zabaleran "
"norabidean, kurbadura eta kolorean alda daiteke. Lerro bat bi dimentsiotakoa "
"(arkatza paperean) edo inplizituki hiru dimentsio izan ditzake."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:145
#, no-wrap
msgid "Shape"
msgstr "Forma"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"Uneko edo inplizitutako lerroak elkartzen direnean hutsune baten inguran, "
"figura laua, forma bat sortzen da. Kolore edo itzaleko aldaketek forma bat "
"defini dezakete. Formak hainbat motetan bana daitezke: geometrikoak "
"(laukizuzena, hirukia, borobila) eta organikoak (ertz irregularrekin)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:197
#, no-wrap
msgid "Size"
msgstr "Tamaina"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"Objektu, lerro edo formen proportzioak aldatzeari deritzo. Objektuen tamaina "
"aldaketa erreala edo irudikaria izan daiteke."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:158
#, no-wrap
msgid "Space"
msgstr "Espazioa"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"Espazioa objektuen artean, inguruan, gainean, azpian edo barruan dagoen "
"azalera huts edo irekia da. Formak inguruko eta barruko espazioekin osatzen "
"dira. Espazioari askotan hiru-dimentsiotakoa edo bi-dimentsiotakoa esaten "
"zaio. Espazio positiboak formen barruan daude. Espazio negatiboek formak "
"inguratzen dituzte."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:119
#, no-wrap
msgid "Color"
msgstr "Kolorea"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"kolorea isladatutako argi uhinaren luzeraren arabera nabaritzen dugun "
"azaleraren ezaugarria da. Koloreak hiru dimentsio ditu: ÑABARDURA "
"(kolorearentzako beste hitz bat bere izenaren arabera zehaztuta: gorria, "
"horia,...), BALIOA (bere argitasun edo iluntasuna), INTENTSITATEA ( bere "
"distira edo matetasuna)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:171
#, no-wrap
msgid "Texture"
msgstr "Testura"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"Testura gainazal batek ematen duen sentimendua da (uneko testura) edo itxura "
"(inplikatutako testura) da. Testurak zakar, zetatsu, edo harritsu bezalako "
"hitzekin deskribatzen dira."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:184
#, no-wrap
msgid "Value"
msgstr "Balioa"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Balioa zerbaiten argi edo iluntasun zenbateko da. Kolorean balio aldaketa "
"zuri edo beltza gehituz lortzen dugu. Chiaroscuro-k marrazkietan argi eta "
"ilunaren arteko kontraste handia sortzean datza, balioa erabiliz."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr "Diseinu arauak"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr "Arauek diseinuko elementuak konposizioak egiteko erabiltzen dituzte."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:210
#, no-wrap
msgid "Balance"
msgstr "Balantzea"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"Balantzea bisualki itxura, forma, balioa, kolorea, eta abarren "
"berdintasunaren sentimendua da. Balantzea simetriko edo maila berekoa ala "
"asimetriko eta maila ezberdinetakoa izan daiteke. Objektuak, balioak, "
"koloreak, testurak, itxurak, formak, eta abar erabil daitezke konposizio "
"batean balantzea sortzeko."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:223
#, no-wrap
msgid "Contrast"
msgstr "Kontrastea"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "Kontrastea kontrako elementuen justaposizioa da"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:236
#, no-wrap
msgid "Emphasis"
msgstr "Nabarmentzea"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"Nabarmentzea zure atentzioa artelanaren eremu zehatz batzuetara gidatzeko "
"erabiltzen da. Begiratzen duzun lehenengo lekua puntu fokala edo interesaren "
"zentrua da"

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:249
#, no-wrap
msgid "Proportion"
msgstr "Proportzioa"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"Proportzioak beste batekin alderatuta gauza baten tamaina, kokapena edo "
"kopurua adierazten du."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:262
#, no-wrap
msgid "Pattern"
msgstr "Patroia"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"Patroia elementu bat behin eta berriz errepikatzean (lerro, forma edo "
"kolore) sortzen da."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:276
#, no-wrap
msgid "Gradation"
msgstr "Mailaketa"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"Tamaina eta norabidearen mailaketak perspektiba lineala sortzen du. Kolore "
"berotik hotzerako eta tonu ilunetik argirako mailaketak perspektiba aereoa "
"sortzen du. Mailaketak interesa eta mugimendua gehi diezaioke forma bati. "
"Ilunetik argirako mailaketak begia formatik barrena mugiaraz dezake."

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:289
#, no-wrap
msgid "Composition"
msgstr "Konposaketa"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr "Elementu ezberdinen konposizioa osotasun bat sortzeko."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr "Bibliiografia"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid ""
"This is a partial bibliography used to build this document (links may no "
"longer work today or link to malicious sites now, click with care)."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:242
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:247
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2."
"htm\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:252
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:257
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:262
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:267
#, fuzzy
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink "
"url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this "
"tutorial. Also, thanks to the Open Clip Art Library (<ulink url=\"https://"
"openclipart.org/\">https://openclipart.org/</ulink>) and the graphics people "
"have submitted to that project."
msgstr ""
"Esker bereziak Linda Kim-eri (<ulink url=\"http://www.redlucite.org\">http://"
"www.redlucite.org</ulink>) neroni (<ulink url=\"http://www.rejon.org/"
"\">http://www.rejon.org/</ulink>) tutorial honekin laguntzeagatik. Baita, "
"eskerrak Open Clip Art Liburutegiari (<ulink url=\"http://www.openclipart."
"org/\">http://www.openclipart.org/</ulink>) eta jendeak proiektu honetara "
"igo dituen grafikoei."

#. (itstool) path: Work/format
#: elements-f01.svg:51 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: elements-f01.svg:92
#, no-wrap
msgid "Elements"
msgstr "Elementuak"

#. (itstool) path: text/tspan
#: elements-f01.svg:106
#, no-wrap
msgid "Principles"
msgstr "Arauak"

#. (itstool) path: text/tspan
#: elements-f01.svg:302
#, no-wrap
msgid "Overview"
msgstr "Gainbegirada"

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr "HANDIA"

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr "txikia"

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Ausazko Inurria eta 4x4"

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "Andrew Fitzsimon-ek Sortutako SVG Irudia"

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Clip Art Liburutegiak Eskeinia"

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, fuzzy, no-wrap
msgid "https://openclipart.org/"
msgstr "http://www.openclipart.org/"

#~ msgid "This is a partial bibliography used to build this document."
#~ msgstr "Hau dokumentua sortzeko bibliografiaren zati bat da."

#~ msgid "http://www.makart.com/resources/artclass/EPlist.html"
#~ msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#~ msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
#~ msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#~ msgid "http://www.johnlovett.com/test.htm"
#~ msgstr "http://www.johnlovett.com/test.htm"

#~ msgid "http://digital-web.com/articles/elements_of_design/"
#~ msgstr "http://digital-web.com/articles/elements_of_design/"

#~ msgid "http://digital-web.com/articles/principles_of_design/"
#~ msgstr "http://digital-web.com/articles/principles_of_design/"

#~ msgid "http://sanford-artedventures.com/study/study.html"
#~ msgstr "http://sanford-artedventures.com/study/study.html"

#~ msgid "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
#~ msgstr "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
