# Translation of Inkscape's calligraphy tutorial to Ukrainian
#
#
# Nazarii Ritter <nazariy.ritter@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2023-07-14 16:46+0200\n"
"PO-Revision-Date: 2017-04-22 23:28+0300\n"
"Last-Translator: Nazarii Ritter <nazariy.ritter@gmail.com>\n"
"Language-Team: \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Nazarii Ritter <nazariy.ritter@gmail.com>, 2017"

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "Каліграфія"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Одним з багатьох чудових інструментів, доступних в «Inkscape» є інструмент "
"«Каліграфія». Цей урок ознайомить Вас з роботою даного інструменту, а також "
"продемонструє деякі базові техніки мистецтва каліграфії."

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"Використовуйте <keycap>Ctrl+стрілки</keycap>, <keycap>коліщатко миші</"
"keycap> або <keycap>перетягування за допомогою середньої клавіші</keycap>, "
"щоб прокрутити сторінку вниз. Для основ створення, вибору та перетворення "
"об'єктів, див. «Початковий рівень» в <command>«Довідка» &gt; «Підручники»</"
"command>."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "Історія та стилі"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"Згідно словника, <firstterm>каліграфія</firstterm> означає «красиве письмо» "
"або «гарний або вишуканий почерк». По суті, каліграфія – це мистецтво "
"красивого чи вишуканого почерку. Це може здаватися складним, але трохи "
"попрактикувавшись, будь-хто може опанувати основи цього мистецтва."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
#, fuzzy
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is in the realm of motivating handlettering wall art, though."
msgstr ""
"Найраніші форми каліграфії беруть початок з часів печерних малюнків. Десь "
"приблизно аж 1440 р. до н.е., до винайдення друкарського преса, каліграфія "
"була методом видання книг та інших видань. Писар мав вручну переписувати "
"кожен окремий екземпляр книги чи видання. Рукопис виконувався за допомогою "
"пера та чорнил на таких матеріалах як пергамент або велень. Стилі письма, що "
"використовувались сторіччями включають рустику (селянське просте капітальне "
"письмо), каролінзький, готичний та ін. Напевно, найчастіше побачити "
"каліграфію сьогодні можна на весільних запрошеннях."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:35
msgid "There are three main styles of calligraphy:"
msgstr "Є три основні стилі каліграфії:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:40
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:46
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:52
#, fuzzy
msgid "Chinese or Oriental, executed with a brush."
msgstr "китайський або східний (азійський)"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:57
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:61
#, fuzzy
msgid ""
"Today, one great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"Однією з великих переваг, яка є в нас над писарями минулого є команда "
"«<command>Вернути</command>»: якщо буде допущено помилку, вся сторінка не "
"буде зіпсованою. Інструмент «Каліграфія» також робить можливими деякі "
"прийоми, недоступні з традиційним пером та чорнилами."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:68
msgid "Hardware"
msgstr "Апаратні засоби"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:69
#, fuzzy
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm>. Thanks to the flexibility of our tool, even those with only a "
"mouse can do some fairly intricate calligraphy, though there will be some "
"difficulty producing fast sweeping strokes."
msgstr ""
"найкращі результати досягаються при використанні <firstterm>планшету та "
"пера</firstterm> (наприклад, Wacom). Завдяки гнучкості нашого інструменту, "
"навіть з використанням самої лише миші можна зробити досить складну "
"каліграфію, хоче будуть певні труднощі зі створенням швидких розмашистих "
"штрихів."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"«Inkscape» здатен використовувати чутливість до <firstterm>сили натиску</"
"firstterm> та <firstterm>нахилу</firstterm> пера планшету, що підтримує дані "
"функції. За замовчуванням, функції – вимкнені, оскільки вони вимагають "
"налаштування. Також зверніть увагу, що каліграфія за допомогою пера чи ручки "
"з пером – не дуже чутлива до сили натиску, на відміну від пензлика."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:80
#, fuzzy
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Для використання функцій чутливості планшета, потрібно налаштувати його. Це "
"налаштування потрібно виконати лише один раз і зберегти параметри "
"налаштування. Щоб увімкнути підтримку даної функції, потрібно підключити "
"планшет перед запуском «Inkscape», а потім відкрити діалогове вікно "
"«<firstterm>Пристрої введення…</firstterm>» з меню «<emphasis>Зміни</"
"emphasis>». В цьому вікні можна вибрати бажаний пристрій та налаштування для "
"пера планшету. І нарешті, після вибору налаштувань, активуйте інструмент "
"«Каліграфія» і перемкніть кнопки для натиску й нахилу. Відтепер, «Inkscape» "
"пам'ятатиме ці налаштування при кожному запуску."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:89
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Каліграфічне перо «Inkscape» може бути чутливим до <firstterm>швидкості</"
"firstterm> штриха (див. «Звуження» нижче), тому при використанні миші, "
"напевно, потрібно виставити даний параметр на нуль."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:95
msgid "Calligraphy Tool Options"
msgstr "Параметри інструменту «Каліграфія»"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:96
#, fuzzy
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing "
"the <keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel>, "
"<guilabel>Thinning</guilabel>, <guilabel>Mass</guilabel>, <guilabel>Angle</"
"guilabel>, <guilabel>Fixation</guilabel>, <guilabel>Caps</guilabel>, "
"<guilabel>Tremor</guilabel> and <guilabel>Wiggle</guilabel>. There are also "
"two buttons to toggle tablet Pressure and Tilt sensitivity on and off (for "
"drawing tablets), as well as a dropdown menu with a few presets to select "
"from, and the option to save your own presets."
msgstr ""
"Щоб активувати інструмент «Каліграфія», натисніть <keycap>Ctrl+F6</keycap>, "
"клавішу <keycap>C</keycap> або клацніть її кнопку на панелі інструментів. "
"На  верхній панелі з'явиться 8 опцій: ширина та звуження; кут та фіксація; "
"кінці; дрижання, погойдування та маса. Також є дві кнопки для вмикання/"
"вимкнення чутливості планшету до сили натиску та нахилу (для графічних "
"планшетів)."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:108
msgid "Width &amp; Thinning"
msgstr "Ширина та звуження"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:109
#, fuzzy
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom "
"(symbolized as %). This makes sense, because the natural “unit of measure” "
"in calligraphy is the range of your hand's movement, and it is therefore "
"convenient to have the width of your pen nib in constant ratio to the size "
"of your “drawing board” and not in some real units which would make it "
"depend on zoom. This behavior is optional though, so it can be changed for "
"those who would prefer absolute units regardless of zoom. To switch the "
"unit, simply use the unit selector after the <guilabel>Unit</guilabel> field."
msgstr ""
"Ці два параметри регулюють <firstterm>ширину</firstterm> пера. Ширина може "
"бути від 1 до 100 і (за замовчуванням) вимірюється одиницях, відносних до "
"вікна редактора, але незалежних від масштабу. Це має значення, тому що "
"природна «одиниця виміру» в каліграфії є розмах руху руки, і тому зручно "
"мати постійним співвідношення ширини пера до розмірів «дошки для малювання», "
"а не якісь залежні від масштабу реальні одиниці виміру. Однак, така "
"поведінка є необов'язковою. Тому, для тих, хто надає перевагу абсолютним "
"одиницям виміру, незалежним від масштабу, її можна змінити. Щоб перемкнути "
"даний режим, використовуйте кнопку-прапорець на сторінці «Налаштування…» "
"інструментів (можна відкрити її двічі клацнувши на кнопці інструменту)."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:118
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Оскільки ширина пера змінюється часто, можна коригувати її без панелі "
"інструментів, а використовуючи клавіші-стрілки «<keycap>вліво</keycap>» та "
"«<keycap>вправо</keycap>» або за допомогою планшету з підтримкою функції "
"чутливості сили натиску. Найкращим є те, що дані клавіші працюють під час "
"малювання. Тому можна поступово змінювати ширину пера посеред штриха."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:131
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"Ширина пера також може залежати від швидкості, що регулюється параметром "
"«<firstterm>Звуження</firstterm>». Даний параметр може приймати значення від "
"-100 до 100; нуль означає, що ширина не залежить від швидкості, додатні "
"значення роблять швидші штрихи тоншими, негативні – ширшими. Значення за "
"замовчуванням 10 означає середнє звуження швидких штрихів. Ось – декілька "
"прикладів. Всі намальовані з шириною = 20 та кутом = 90:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:144
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Для забавки, виставте і ширину і звуження на 100 (максимум) і малюйте "
"рвучкими рухами, щоб отримати дивні натуралістичні, нейроноподібні фігури:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:157
msgid "Angle &amp; Fixation"
msgstr "Кут та фіксація"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:158
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"Після ширини, <firstterm>кут</firstterm> – найважливіший параметр "
"каліграфії. Це – кут пера в градусах, від 0 (горизонтальний) до 90 "
"(вертикальний проти годинникової стрілки) або до -90 (вертикальний за "
"годинниковою стрілкою). Зверніть увагу, що, якщо увімкнути чутливість "
"планшету до нахилу, то параметр «Кут» стане недоступним і кут буде "
"визначатися нахилом пера."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:171
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Кожен традиційний стиль каліграфії має власний поширений кут нахилу пера. "
"Наприклад, унціальне письмо використовує кут в 25°. Складніші види письма та "
"досвідченіші каліграфісти часто змінюють кут під час малювання, і «Inkscape» "
"робить це можливим при натисканні клавіш-стрілок «<keycap>вгору</keycap>» та "
"«<keycap>вниз</keycap>» або за допомогою планшету з функцією чутливості до "
"нахилу. Для початкових уроків з каліграфії, краще всього залишати кут "
"постійним. Ось – приклади штрихів намальованих з різними кутами "
"(фіксація = 100):"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:185
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Як видно, штрих найтонший, коли намальований паралельно до свого кута, і "
"найширший – коли перпендикулярно. Додатні кути – більш природні та "
"традиційні для каліграфії для правої руки."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:189
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Рівень відмінності між найтоншим та найтовщим регулюється параметром "
"«<firstterm>Фіксація</firstterm>». Значення 100 означає, що кут завжди "
"постійний, як встановлено в полі «Кут». Зменшення фіксації дозволяє перу "
"трохи повертатися проти напрямку штриха. При фіксації = 0 – перо вільно "
"повертається, щоб завжди бути перпендикулярно до штриха, і «Кут» вже не "
"матиме жодного ефекту:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:202
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Кажучи мовою типографії, максимальна фіксація і, таким чином, максимальне "
"співвідношення ширини штриха (зліва вгорі) – притаманні античним шрифтам з "
"засічками, таким як «Times» чи «Bodoni» (оскільки, історично, ці шрифти "
"копіювали каліграфію з фіксованим пером). Нульова фіксація або нульове "
"співвідношення ширини (справа вгорі), з іншого боку, нагадують сучасні "
"шрифти без засічок, такі як «Helvetica»."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:210
msgid "Tremor"
msgstr "Дрижання"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:211
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Дрижання</firstterm> призначене для надання більш природного "
"вигляду каліграфічним штрихам. Дрижання регулюється значеннями від 0 до 100 "
"на панелі керування. Це створює все від легкої непрямолінійності до значної "
"кількості невеликих та великих плям. Це значно розширює творчі межі "
"інструменту."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:226
msgid "Wiggle &amp; Mass"
msgstr "Погойдування та маса"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:227
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"На відміну від ширини та кута, ці два параметри визначають «відчуття» "
"штриха, аніж впливають на його зовнішній вигляд. Тому в цьому розділі не "
"буде жодних ілюстрацій; просто спробуйте їх самі, щоб зрозуміти краще."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:232
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Погойдування</firstterm> – опір паперу руху пера. За "
"замовчуванням, виставлено мінімум (0) і збільшення цього параметру робить "
"папір «слизьким»: якщо маса велика, то перо намагається відхилитися на "
"різких поворотах; якщо маса нульова, то великі значення погойдування змушує "
"перо шалено погойдуватися."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:237
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"У фізиці, «<firstterm>маса</firstterm>» – це те, що спричинює інерцію; чим "
"більша маса інструменту «Каліграфія» в «Inkscape», тим більше він відстає "
"від вказівника миші і тим більше згладжуються гострі повороти і швидкі ривки "
"штриха. За замовчуванням, це значення досить невелике (2), то цей інструмент "
"швидкий і швидко реагує, однак, можна збільшити масу, щоб отримати "
"повільніше та плавніше перо."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:245
msgid "Calligraphy examples"
msgstr "Приклади каліграфії"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:246
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Тепер, ознайомившись з базовими можливостями інструменту, можна спробувати "
"створити справжню каліграфію. Якщо це мистецтво є новим для Вас, знайдіть "
"добру книгу з каліграфії і вчіться по ній разом з «Inkscape». В цьому "
"розділі буде показано лише декілька простих прикладів."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:251
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Перш за все, для створення літер, потрібно створити пару напрямних лінійок. "
"Якщо Ви збираєтеся писати шрифтом з нахилом або курсивом, додайте також і "
"похилих напрямних через дві лінійки, наприклад:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:262
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Потім збільште масштаб так, що висота між лінійками максимально відповідає "
"звичайному розмаху руху Вашої руки, налаштуйте ширину та кут, і вперед!"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:266
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Напевно, найперша річ, яку Ви зробите як каліграфіст-початківець – "
"попрактикуєтесь в написанні базових елементів літер – вертикальних, "
"горизонтальних, округлих та похилих штрихів. Ось – деякі елементи "
"унціального шрифту:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:278
msgid "Several useful tips:"
msgstr "Декілька корисних порад:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
#, fuzzy
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Якщо Ваша рука зручно лежить на планшеті – не ворушіть нею. Замість цього, "
"прокручуйте полотно (<keycap>Ctrl+стрілки</keycap> keys) лівою рукою після "
"завершення кожної літери."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:290
#, fuzzy
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, "
"if its shape is good but the position or size are slightly off, it's better "
"to switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Якщо останній штрих – невдалий, просто відмініть його (<keycap>Ctrl+Z</"
"keycap>). Однак, якщо його форма задовільна, але положення або розмір трохи "
"не такі, то краще тимчасово активувати «Селектор» (<keycap>Пробіл</keycap>) "
"і підштовхнути/відмасштабувати/повернути його як потрібно (за допомогою миші "
"чи клавіатури), а потім знову натиснути <keycap>Пробіл</keycap> і "
"повернутися до інструменту «Каліграфія»."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:299
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Написавши слово, знову активуйте «Селектор», щоб рівномірно підлаштувати "
"штрихи літер і відстані між літерами. Але не перестарайтеся; гарна "
"каліграфія має мати дещо нерівномірний рукописний вигляд. Утримуйтеся від "
"спокуси копіювання літер та їх елементів; кожен штрих має бути унікальним."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:306
msgid "And here are some complete lettering examples:"
msgstr "Ось – декілька довершених прикладів:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:318
msgid "Conclusion"
msgstr "Завершення"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:319
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"Каліграфія – не лише забавка; це – глибоко духовне мистецтво, що може "
"змінити Ваш погляд на все, що Ви робите і бачите. Каліграфічний інструмент "
"«Inkscape» може послугувати в якості скромного вступу. І все ж, з ним "
"приємно розважатися і він може бути корисним в справжній дизайнерській "
"роботі. Насолоджуйтеся!"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "ширина=1, збільшуючись....             досягає 47, зменшуючись...                        знову до 0"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "звуження = 0 (постійна ширина) "

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, no-wrap
msgid "thinning = 10"
msgstr "звуження = 10"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, no-wrap
msgid "thinning = 40"
msgstr "звуження = 40"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, fuzzy, no-wrap
msgid "thinning = −20"
msgstr "звуження = -20"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, fuzzy, no-wrap
msgid "thinning = −60"
msgstr "звуження = -60"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "кут = 90°"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "кут = 30° (за замовчуванням)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "кут = 0°"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, fuzzy, no-wrap
msgid "angle = −90 deg"
msgstr "кут = 90°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "кут = 30°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "кут = 60°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "кут = 90°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "кут = 15°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, fuzzy, no-wrap
msgid "angle = −45"
msgstr "кут = -45°"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, no-wrap
msgid "fixation = 100"
msgstr "фіксація = 100°"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, no-wrap
msgid "fixation = 80"
msgstr "фіксація = 80°"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "фіксація = 0°"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "повільно"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "середньо"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "швидко"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "дрижання = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, no-wrap
msgid "tremor = 10"
msgstr "дрижання = 10"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, no-wrap
msgid "tremor = 30"
msgstr "дрижання = 30"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, no-wrap
msgid "tremor = 50"
msgstr "дрижання = 50"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, no-wrap
msgid "tremor = 70"
msgstr "дрижання = 70"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, no-wrap
msgid "tremor = 90"
msgstr "дрижання = 90"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, no-wrap
msgid "tremor = 20"
msgstr "дрижання = 20"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, no-wrap
msgid "tremor = 40"
msgstr "дрижання = 40"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, no-wrap
msgid "tremor = 60"
msgstr "дрижання = 60"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, no-wrap
msgid "tremor = 80"
msgstr "дрижання = 80"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, no-wrap
msgid "tremor = 100"
msgstr "дрижання = 100"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, fuzzy, no-wrap
msgid "Uncial hand"
msgstr "Унціальне письмо"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "Каролінзьке письмо"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "Готичне письмо"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "Бастардське письмо"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Розмашисте курсивне письмо"

#~ msgid "Western or Roman"
#~ msgstr "західний або романський"

#~ msgid "Arabic"
#~ msgstr "арабський"

#~ msgid ""
#~ "This tutorial focuses mainly on Western calligraphy, as the other two "
#~ "styles tend to use a brush (instead of a pen with nib), which is not how "
#~ "our Calligraphy tool currently functions."
#~ msgstr ""
#~ "Даний урок, в основному, приділяє увагу західній каліграфії, оскільки "
#~ "інші два стилі схильні до використання пензля (замість ручки з пером), "
#~ "який пише не так як інструмент «Каліграфія» функціонує."

#~ msgid "angle = -90 deg"
#~ msgstr "кут = -90°"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "bulia byak, buliabyak@users.sf.net та josh andler, scislac@users.sf.net"
