<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<book>
  <article id="TIPSANDTRICKS">
    <articleinfo>
      <title>Tips and Tricks</title>
      <subtitle>Tutorial</subtitle>
      <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="tutorial-tips-authors.xml" />
    </articleinfo>
    <abstract>
      <para>
        This tutorial will demonstrate various tips and tricks that users have learned through the use of Inkscape and
        some “hidden” features that can help you speed up production tasks.
      </para>
    </abstract>
    <sect1>
      <title>Radial placement with Tiled Clones</title>
      <para>
        It's easy to see how to use the <guimenuitem>Create Tiled Clones</guimenuitem> dialog for rectangular grids and
        patterns. But what if you need <firstterm>radial</firstterm> placement, where objects share a common center of
        rotation? It's possible too!
      </para>
      <para>
        If your radial pattern only needs to have 3, 4, 6, 8, or 12 elements, then you can try the P3, P31M, P3M1, P4,
        P4M, P6, or P6M symmetries. These will work nicely for snowflakes and the like. A more general method, however,
        is as follows.
      </para>
      <para>
        Choose the P1 symmetry (simple translation) and then <emphasis>compensate</emphasis> for that translation by
        going to the <guimenuitem>Shift</guimenuitem> tab and setting <guilabel>Per row/Shift Y</guilabel> and
        <guilabel>Per column/Shift X</guilabel> both to -100%. Now all clones will be stacked exactly on top of the
        original. All that remains to do is to go to the <guimenuitem>Rotation</guimenuitem> tab and set some rotation
        angle per column, then create the pattern with one row and multiple columns. For example, here's a pattern made
        out of a horizontal line, with 30 columns, each column rotated 6 degrees:
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="tips-f01.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
      <para>
        To get a clock dial out of this, all you need to do is cut out or simply overlay the central part by a white
        circle (to do boolean operations on multiple clones at once, combine them first).
      </para>
      <para>
        More interesting effects can be created by using both rows and columns. Here's a pattern with 10 columns and 8
        rows, with rotation of 2 degrees per row and 18 degrees per column. Each group of lines here is a “column”,
        so the groups are 18 degrees from each other; within each column, individual lines are 2 degrees apart:
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="tips-f02.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
      <para>
        In the above examples, the line was rotated around its center. But what if you want the center to be outside of
        your shape? Just click on the object twice with the Selector tool to enter rotation mode. Now move the object's
        rotation center (represented by a small cross-shaped handle) to the point you would like to be the center of the
        rotation for the Tiled Clones operation. Then use <guimenuitem>Create Tiled Clones</guimenuitem> on the object.
        This is how you can do nice “explosions” or “starbursts” by randomizing scale, rotation, and possibly
        opacity:
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="tips-f03.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
    </sect1>
    <sect1>
      <title>Non-linear gradients</title>
      <para>
        The version 1.1 of SVG does not support non-linear gradients (i.e. those which have a non-linear translations
        between colors). You can, however, emulate them by <firstterm>multistop</firstterm> gradients.
      </para>
      <para>
        Start with a simple two-stop gradient (you can assign that in the Fill and Stroke dialog or use the gradient
        tool). Now, with the gradient tool, add a new gradient stop in the middle; either by double-clicking on the
        gradient line, or by selecting the square-shaped gradient stop and clicking on the button <guimenuitem>Insert
        new stop</guimenuitem> in the gradient tool's tool bar at the top. Drag the new stop a bit. Then add more stops
        before and after the middle stop and drag them too, so that the gradient looks smooth. The more stops you add,
        the smoother you can make the resulting gradient. Here's the initial black-white gradient with two stops:
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="tips-f04.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
      <para>
        And here are various “non-linear” multi-stop gradients (examine them in the Gradient Editor):
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="tips-f05.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
    </sect1>
    <sect1>
      <title>Excentric radial gradients</title>
      <para>
        Radial gradients don't have to be symmetric. In Gradient tool, drag the central handle of an elliptic gradient
        with <keycap function="shift">Shift</keycap>. This will move the x-shaped <firstterm>focus handle</firstterm> of
        the gradient away from its center. When you don't need it, you can snap the focus back by dragging it close to
        the center.
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="tips-f06.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
    </sect1>
    <sect1>
      <title>Mesh gradients</title>
      <para>
        For even more complex gradients, Inkscape offers the <firstterm>Mesh Gradient tool</firstterm> (the tool right
        below the gradient tool in the tool bar). Set the shape (radial or grid) and the number of rows and columns of
        color patches in the tool controls bar, and then use the tool to drag over any shape to fill it with a mesh
        gradient.
      </para>
      <para>
        Now you can adjust the shape of the color patches with the triangular handles, and select the diamond-shaped
        handles to change the patches' color. You can even pick the color of objects below the mesh gradient by clicking
        on the color picker icon in its tool controls.
      </para>
    </sect1>
    <sect1>
      <title>Aligning to the center of the page</title>
      <para>
        To align something to the center or side of a page, select the object or group and then choose
        <guimenuitem>Page</guimenuitem> from the <guilabel>Relative to:</guilabel> list in the <guimenuitem>Align and
        Distribute</guimenuitem> dialog
        (<keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>A</keycap></keycombo>).
      </para>
    </sect1>
    <sect1>
      <title>Cleaning up the document</title>
      <para>
        Many of the no-longer-used gradients, patterns, and markers (more precisely, those which you edited manually)
        remain in the corresponding palettes and can be reused for new objects. However if you want to optimize your
        document, use the <guimenuitem>Clean Up Document</guimenuitem> command in <guimenu>File</guimenu> menu. It will
        remove any gradients, patterns, or markers which are not used by anything in the document, making the file
        smaller.
      </para>
    </sect1>
    <sect1>
      <title>Hidden features and the XML editor</title>
      <para>
        The XML editor
        (<keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>X</keycap></keycombo>)
        allows you to change almost all aspects of the document without using an external text editor. Also, Inkscape
        usually supports more SVG features than are accessible from the GUI. The XML editor is one way to get access to
        these features (if you know SVG).
      </para>
    </sect1>
    <sect1>
      <title>Changing the rulers' unit of measure</title>
      <para>
        In the default template, the unit of measure used by the rulers is mm. This is also the unit used in displaying
        coordinates at the lower-right corner and preselected in all units menus. (You can always hover your
        <mousebutton>mouse</mousebutton> over a ruler to see the tooltip with the units it uses.) To change this, open
        <guimenuitem>Document Properties</guimenuitem>
        (<keycombo><keycap function="shift">Shift</keycap><keycap function="control">Ctrl</keycap><keycap>D</keycap></keycombo>)
        and change the <guimenuitem>Display units</guimenuitem> on the <guimenuitem>Display</guimenuitem> tab.
      </para>
    </sect1>
    <sect1>
      <title>Stamping</title>
      <para>
        To quickly create many copies of an object, use <firstterm>stamping</firstterm>. Just drag an object (or scale
        or rotate it), and while holding the <mousebutton role="click">mouse</mousebutton> button down, press
        <keycap>Space</keycap>. This leaves a “stamp” of the current object shape. You can repeat it as many times
        as you wish.
      </para>
    </sect1>
    <sect1>
      <title>Pen tool tricks</title>
      <para>
        In the Pen (Bezier) tool, you have the following options to finish the current line:
      </para>
      <itemizedlist>
        <listitem>
          <para>
            Press <keycap>Enter</keycap>
          </para>
        </listitem>
        <listitem>
          <para>
            <mousebutton role="double-click">Double click</mousebutton> with the left mouse button
          </para>
        </listitem>
        <listitem>
          <para>
            Click with the <mousebutton role="right-click">right</mousebutton> mouse button
          </para>
        </listitem>
        <listitem>
          <para>
            Select another tool
          </para>
        </listitem>
      </itemizedlist>
      <para>
        Note that while the path is unfinished (i.e. is shown green, with the current segment red) it does not yet exist
        as an object in the document. Therefore, to cancel it, use either <keycap>Esc</keycap> (cancel the whole path)
        or <keycap>Backspace</keycap> (remove the last segment of the unfinished path) instead of
        <guimenuitem>Undo</guimenuitem>.
      </para>
      <para>
        To add a new subpath to an existing path, select that path and start drawing with
        <keycap function="shift">Shift</keycap> from an arbitrary point. If, however, what you want is to simply
        <emphasis>continue</emphasis> an existing path, Shift is not necessary; just start drawing from one of the end
        anchors of the selected path.
      </para>
    </sect1>
    <sect1>
      <title>Entering Unicode values</title>
      <para>
        While in the Text tool, pressing <keycombo><keycap function="control">Ctrl</keycap><keycap>U</keycap></keycombo>
        toggles between Unicode and normal mode. In Unicode mode, each group of 4 hexadecimal digits you type becomes a
        single Unicode character, thus allowing you to enter arbitrary symbols (as long as you know their Unicode
        codepoints and the font supports them). To finish the Unicode input, press <keycap>Enter</keycap>. For example,
        <keycombo action="seq"><keycap function="control">Ctrl</keycap><keycap>U</keycap><keycap>2</keycap><keycap>0</keycap><keycap>1</keycap><keycap>4</keycap><keycap>Enter</keycap></keycombo>
        inserts an em-dash (&#8212;). To quit the Unicode mode without inserting anything press <keycap>Esc</keycap>.
      </para>
      <para>
        You can also use the <menuchoice><guimenu>Text</guimenu><guimenuitem>Unicode
        Characters</guimenuitem></menuchoice> dialog to search for and insert glyphs into your document.
      </para>
    </sect1>
    <sect1>
      <title>Using the grid for drawing icons</title>
      <para>
        Suppose you want to create a 24x24 pixel icon. Create a 24x24 px canvas (use the <guimenuitem>Document
        Preferences</guimenuitem>) and set the grid to 0.5 px (48x48 gridlines). Now, if you align filled objects to
        <emphasis>even</emphasis> gridlines, and stroked objects to <emphasis>odd</emphasis> gridlines with the stroke
        width in px being an even number, and export it at the default 96dpi (so that 1 px becomes 1 bitmap pixel), you
        get a crisp bitmap image without unneeded antialiasing.
      </para>
    </sect1>
    <sect1>
      <title>Object rotation</title>
      <para>
        When in the Selector tool, <mousebutton role="click">click</mousebutton> on an object to see the scaling arrows,
        then <mousebutton role="click">click</mousebutton> again on the object to see the rotation and skew arrows. If
        the arrows at the corners are clicked and dragged, the object will rotate around the center (shown as a cross
        mark). If you hold down the <keycap function="shift">Shift</keycap> key while doing this, the rotation will
        occur around the opposite corner. You can also drag the rotation center to any place.
      </para>
      <para>
        Or, you can rotate from keyboard by pressing <keycap>[</keycap> and <keycap>]</keycap> (by 15 degrees) or
        <keycombo><keycap function="control">Ctrl</keycap><keycap>[</keycap></keycombo> and
        <keycombo><keycap function="control">Ctrl</keycap><keycap>]</keycap></keycombo> (by 90 degrees). The same
        <keycap>[</keycap> <keycap>]</keycap> keys with <keycap function="alt">Alt</keycap> perform slow pixel-size
        rotation.
      </para>
    </sect1>
    <sect1>
      <title>Drop shadows</title>
      <para>
        To quickly create drop shadows for objects, use the <menuchoice><guimenu>Filters</guimenu><guisubmenu>Shadows
        and Glows</guisubmenu><guimenuitem>Drop Shadow</guimenuitem></menuchoice> feature.
      </para>
      <para>
        You can also easily create blurred drop shadows for objects manually with blur in the Fill and Stroke dialog.
        Select an object, duplicate it by
        <keycombo><keycap function="control">Ctrl</keycap><keycap>D</keycap></keycombo>, press <keycap>PgDown</keycap>
        to put it beneath original object, place it a little to the right and lower than original object. Now open Fill
        And Stroke dialog and change Blur value to, say, 5.0. That's it!
      </para>
    </sect1>
    <sect1>
      <title>Placing text on a path</title>
      <para>
        To place text along a curve, select the text and the curve together and choose <guimenuitem>Put on
        Path</guimenuitem> from the <guimenu>Text</guimenu> menu. The text will start at the beginning of the path. In
        general it is best to create an explicit path that you want the text to be fitted to, rather than fitting it to
        some other drawing element &#8212; this will give you more control without screwing over your drawing.
      </para>
    </sect1>
    <sect1>
      <title>Selecting the original</title>
      <para>
        When you have a text on path, a linked offset, or a clone, their source object/path may be difficult to select
        because it may be directly underneath, or made invisible and/or locked. The magic key
        <keycombo><keycap function="shift">Shift</keycap><keycap>D</keycap></keycombo> will help you; select the text,
        linked offset, or clone, and press
        <keycombo><keycap function="shift">Shift</keycap><keycap>D</keycap></keycombo> to move selection to the
        corresponding path, offset source, or clone original.
      </para>
    </sect1>
    <sect1>
      <title>Window off-screen recovery</title>
      <para>
        When moving documents between systems with different resolutions or number of displays, you may find Inkscape
        has saved a window position that places the window out of reach on your screen. Simply maximise the window
        (which will bring it back into view, use the task bar), save and reload. You can avoid this altogether by
        unchecking the global option to save window geometry (<guimenuitem>Inkscape Preferences</guimenuitem>,
        <menuchoice><guimenu>Interface</guimenu><guimenuitem>Windows</guimenuitem></menuchoice> section).
      </para>
    </sect1>
    <sect1>
      <title>Transparency, gradients, and PostScript export</title>
      <para>
        PostScript or EPS formats do not support <emphasis>transparency</emphasis>, so you should never use it if you
        are going to export to PS/EPS. In the case of flat transparency which overlays flat color, it's easy to fix it:
        Select one of the transparent objects; switch to the Dropper tool (<keycap>F7</keycap> or <keycap>D</keycap>);
        make sure that the <guilabel>Opacity: Pick</guilabel> button in the dropper tool's tool bar is deactivated;
        click on that same object. That will pick the visible color and assign it back to the object, but this time
        without transparency. Repeat for all transparent objects. If your transparent object overlays several flat color
        areas, you will need to break it correspondingly into pieces and apply this procedure to each piece. Note that
        the dropper tool does not change the opacity value of the object, but only the alpha value of its fill or stroke
        color, so make sure that every object's opacity value is set to 100% before you start out.
      </para>
    </sect1>
    <sect1>
      <title>Interactivity</title>
      <para>
        Most SVG elements can be tweaked to react to user input (usually this will only work if the SVG is displayed in
        a web browser).
      </para>
      <para>
        The simplest possibility is to add a clickable link to objects. For this
        <mousebutton role="right-click">right-click</mousebutton> the object and select <menuchoice><guimenuitem>Create
        Link</guimenuitem></menuchoice> from the context menu. The "Object attributes" dialog will open, where you can
        set the target of the link using the value of <guilabel>href</guilabel>.
      </para>
      <para>
        More control is possible using the interactivity attributes accessible from the "Object Properties" dialog
        (<keycombo><keycap function="control">Ctrl</keycap><keycap function="shift">Shift</keycap><keycap>O</keycap></keycombo>).
        Here you can implement arbitrary functionality using JavaScript. Some basic examples:
      </para>
      <itemizedlist>
        <listitem>
          <para>
            Open another file in the current window when clicking on the object:
          </para>
          <itemizedlist>
            <listitem>
              <para>
                Set <guilabel>onclick</guilabel> to <code>window.location='file2.svg';</code>
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
        <listitem>
          <para>
            Open an arbitrary weblink in new window when clicking on the object:
          </para>
          <itemizedlist>
            <listitem>
              <para>
                Set <guilabel>onclick</guilabel> to <code>window.open("https://inkscape.org","_blank");</code>
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
        <listitem>
          <para>
            Reduce transparency of the object while hovering:
          </para>
          <itemizedlist>
            <listitem>
              <para>
                Set <guilabel>onmouseover</guilabel> to <code>style.opacity = 0.5;</code>
              </para>
            </listitem>
            <listitem>
              <para>
                Set <guilabel>onmouseout</guilabel> to <code>style.opacity = 1;</code>
              </para>
            </listitem>
          </itemizedlist>
        </listitem>
      </itemizedlist>
    </sect1>
  </article>
</book>
